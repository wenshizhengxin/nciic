<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022-01-18
 * Time: 9:12
 */
use wenshizhengxin\nciic\Nciic;

require_once __DIR__.'/../vendor/autoload.php';
Nciic::init("使用单位","秘钥","wsdl文档路径");
try{
    // 单条
    $res = Nciic::queryOne("张三","45646545616","012456","个人贷款");
    var_dump($res);
    $data = [
        ["name"=>"张三","idcard"=>"15156565665"],
        ["name"=>"lisi","idcard"=>"789465464"],
    ];
    // 多条
    $res = Nciic::queryMultiple($data,"45456","个人贷款");
    var_dump($res);
}catch (\Exception $e){
    echo $e->getMessage();
}

