<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2022-01-14
 * Time: 11:07
 */

namespace wenshizhengxin\nciic;


class Nciic
{
    private static $shiYongDanWei = "";
    private static $inLicense = "";
    private static $wsdlPath = "";

    /**
     * @param $shiYongDanWei String 即使用单位：添加真正使用核查比对服务的公司、单位、机构名
    称。如果是子公司、下属单位使用服务，则填写子公司、下属单位的名称全
    称。最多可输入 10 个汉字。 例如：工商银行北京分行东城区支行。
     * @param $inLicense String 用于填写用户名、口令信息，直接填写授权文件的密文文字既可。授权文
    件由全国公民身份证号码查询服务中心提供。
     */
    public static function init($shiYongDanWei,$inLicense,$wsdlPath){
        self::$shiYongDanWei = $shiYongDanWei;
        self::$inLicense = $inLicense;
        self::$wsdlPath = $wsdlPath;
    }

    /**
     * @param $name String 姓名
     * @param $idcard String 身份证号
     * @param $ywlx String 业务类型
     * 根据各个行业的具体业务环节来确定使用身份信
    息核查比对服务的具体业务类型。最多可输入 20 个汉字。例如：个人贷款
     * @param $fsd String 即业务发生地 被核查比对的公民办理业务的实际所在地行政区
    划代码，精确到区级或县级。格式为：6 位数字。例如：业务发生地是北京
    市海淀区，则填写为“110108”
     * @return mixed
     */
    public static function queryOne($name,$idcard,$fsd,$ywlx){
        $data = [
            ["name"=>$name,"idcard"=>$idcard]
        ];
        return self::queryMultiple($data,$fsd,$ywlx);
    }

    /**
     * @param $dataList array 包含 name idcard 字段的二维数组
     * @param $fsd String 发生地 见queryOne同字段解释
     * @param $ywlx String 业务类型 见queryOne同字段解释
     * @return mixed
     */
    public static function queryMultiple($dataList,$fsd,$ywlx){
        if(!self::check()){
            throw new \Exception("请检查初始化的数据");
        }
        $xml = self::getXml($dataList,$fsd,$ywlx);
        if($xml===false){
            throw new \Exception("传入的数据姓名或身份证号格式不正确");
        }
        $context = stream_context_create([
            'ssl' => [
                // set some SSL/TLS specific options
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]);
        // 去请求接口
        $soapClient = new \SoapClient(self::$wsdlPath,array(
            'stream_context' => $context
        ));
        $data=[
            "inLicense"=>self::$inLicense,
            "inConditions"=>$xml
        ];
        $res = $soapClient->nciicCheck($data);
        $res = json_encode($res);
        $res = json_decode($res,true);
        return self::parseXml($res['out'],count($dataList)>1);
    }
    public static function parseXml($xmlString,$isMultiple=false){
        if(!$xmlString){
           throw new \Exception("返回数据格式异常");
        }
        // 分正常 和 异常 两种
        $xml = simplexml_load_string($xmlString);
        $json = json_encode($xml);
        $array = json_decode($json,true);
        $result = [
            'errors'=>[]
        ];

        // 包含 countrows 是异常报文
        if(strpos($xmlString,"countrows")){ // 异常报文
            foreach ($array["ROWS"] as $row){
                $result['errors'][] = $row['ErrorMsg'];
            }
        }else{ //正常报文 正常报文里也有异常结果
            $foreachData = $array;
            if($isMultiple){
                $foreachData=$array["ROW"];
            }
            foreach ($foreachData as $k=>$row){
                $idcard = $row['INPUT']['gmsfhm'];
                $item = $row['OUTPUT']['ITEM'];
                $errormesagecol = is_string($item[1]['errormesagecol'])?":".$item[1]['errormesagecol']:'';
                $result[$idcard] = [
                    'result_gmsfhm'=>isset($item['0']['result_gmsfhm'])?$item[0]['result_gmsfhm']:'',
                    'result_xm'=>isset($item[1]['result_xm'])?$item[1]['result_xm']:'',
                    'error_msg'=>isset($item[0]['errormesage'])?$item[0]['errormesage'].$errormesagecol:'',
                ];

            }
        }
        return $result;
    }
    private static function check(){
        if(!self::$shiYongDanWei || !self::$inLicense){
            return false;
        }
        return true;
    }
    private static function getXml($data,$fsd,$ywlx){
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><ROWS />');
        $info = $xml->addChild("INFO");
        $info->addChild("SBM",self::$shiYongDanWei);
        $row = $xml->addChild("ROW");
        $row->addChild("GMSFHM","公民身份号码");
        $row->addChild("XM","姓名");

        foreach ($data as $one){
            if(!$one['idcard'] || !is_string($one['idcard']) || !$one['name'] || !is_string($one['name'])){
                return false;
            }
            $row = $xml->addChild("ROW");
            $row->addAttribute("FSD",$fsd);
            $row->addAttribute("YWLX",$ywlx);
            $row->addChild("GMSFHM",$one['idcard']);
            $row->addChild("XM",$one['name']);
        }
        return $xml->asXML();
    }
    // 实际不用 错误描述都返回了
    private static function getError($num){
        $arr = [
            -10=>'数据库连接失败',
            -20=>'写入日志失败',
            -30=>'预付费扣费失败，请检查帐户余额',
            -31=>'预付费用不足',
            -40=>'不是有效的XML文件',
            -41=>'不是有效的XML文件格式',
            -42=>'条件XML文件解析异常',
            -50=>'服务不存在',
            -51=>'服务不是接口方式',
            -52=>'条件中节点与服务定义不一致',
            -53=>'您没有权限使用此服务',
            -54=>'提交的记录超过限制',
            -55=>'构造服务条件失败',
            -56=>'服务类别与调用的接口函数不符',
            -60=>'服务条件项节点缺失',
            -64=>'服务识别码信息不能为空（用户唯一识别码）',
            -70=>'系统繁忙或用户不存在',
            -71=>'用户密码错误',
            -72=>'IP地址受限',
            -80=>'授权文件格式错误',
            -90=>'系统故障，请与管理员联系',
        ];

    }

}